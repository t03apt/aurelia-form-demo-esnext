import 'bootstrap';

const myConfiguration = {
  'aurelia-form': {
    // The default element used, probably best left unchanged
    defaultElement: 'input',

    // The default behavior to use for forms
    defaultBehavior: 'regular',

    // Register your (custom) elements here
    elements: {
      // Example
      'custom-longtext': 'custom-longtext'
    },

    // Configured by aurelia-form-validation
    validation: {},

    // Submit button configuration for <aurelia-form /> and <entity-form />
    submitButton: {
      enabled: true,        // Show the button
      options: ['primary'], // Options to pass to the button
      label: 'Submit'     // Default (fallback) label of the button
    },

    // Alias these entity types to elements (used by aurelia-orm)
    aliases: {
      enum: 'radio',
      int: 'input',
      integer: 'input',
      number: 'input',
      float: 'input',
      string: 'input',
      bool: 'checkbox',
      boolean: 'checkbox',
      text: 'textarea'
    }
  }
};


export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .feature('resources')
    .plugin('aurelia-validation')
    .plugin('aurelia-view-manager')
    .plugin('aurelia-config', config => {
      return config([
        'aurelia-form'
        // Other plugins
      ], myConfiguration);
    })
    .plugin('aurelia-form-validation');

  //Uncomment the line below to enable animation.
  //aurelia.use.plugin('aurelia-animator-css');
  //if the css animator is enabled, add swap-order="after" to all router-view elements

  //Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  //aurelia.use.plugin('aurelia-html-import-template-loader')

  aurelia.start().then(() => aurelia.setRoot());
}
