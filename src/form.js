import { inject, NewInstance } from 'aurelia-framework';
import { position, inputType, element } from 'aurelia-form';
import { ValidationRules, ValidationController } from 'aurelia-validation';

@inject(NewInstance.of(ValidationController))
export class Form {

  constructor(validationController) {
    this.user = {
      email: null,
      password: '1234'
    };

    ValidationRules
      .ensure('email').required()
      .ensure('password').required()
      .on(this.user);

    this.person = new Person();

    ValidationRules
      .ensure((o) => o.name).required()
      .ensure((o) => o.email).required()
      .on(this.person);
  }

  submit() {
    console.log('submit called');
  }
}

export class Person {
  @position(1)
  name = '';

  @position(2)
  @inputType('number')
  age = 0;

  @position(0)
  @inputType('email')
  email = '';

  @position(4)
  @inputType('text')
  @element('custom-longtext')
  description = '';
}
