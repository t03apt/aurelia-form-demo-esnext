export class App {
  configureRouter(config, router) {
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'form'], name: 'form', moduleId: './form', nav: true, title: 'Form' },
    ]);

    this.router = router;
  }
}
