import { bindable, bindingMode, customElement } from 'aurelia-framework';

@customElement('custom-longtext')
export class LongTextCustomElement {
  @bindable({ defaultBindingMode: bindingMode.twoWay }) value = '';
  @bindable editable = true;

  constructor() {
  }

  attached() {
  }
}
